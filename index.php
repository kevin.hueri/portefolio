<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portfolio</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=New+Tegomin&display=swap" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
</head>
<body>

<!--Header-->
<header>
    <div class="logo">
        <h1 class="titrePrincipal">Kevin HUERI</h1>
    </div>
    <div id="aboutMe">
        <p class="textline">About me</p>
        <a href="#apropos"><img id="bottom" src="images/right.svg" alt="Bottom"></a>
    </div>
</header>

<!--Informations-->
<div id="apropos">
    <img id="cornerHautAbout" src="images/Corner.svg" alt="angle">
    <h2 class="aboutTitle">ABOUT ME</h2>
    <div class="avatar">
        <img src="images/moi_bleu.jpg" alt="avatar">
    </div>
    <div class="textAboutMe">
        <h3 id="presentationTitle">Hello, I'm Kevin</h3><br>
        <h3 id="presentationTitle2">& I'm Web Developer</h3>
        <p class="paragrapheAPropos">
            Holder of a BTS in Industrial Maintenance, I have 10 years of professional experience in industry.</p>
        <p class="paragrapheAPropos">
            Following all this experience, I decided to reorient myself. I chose a direction in the digital field,
            and more precisely in the field of web development where I discovered an exciting and innovative path.
        </p>
    </div>
    <div class="resume">
        <a target="_blank" href="images/CV-Kevin-en.pdf">Resume</a>
    </div>
    <div id="downProjects">
        <p class="textline2">Projects</p>
        <a href="#folio"><img src="images/down-project.svg" alt="project"></a>
    </div>
    <img id="cornerBasAbout" src="images/Corner.svg" alt="angle">
</div>

<!--Quelques projets-->
<div id="folio">
    <h2 class="projectTitle">PROJECTS</h2>
    <div id="appli">

<!--        Projet 1-->
        <div class="conteneurProjet">
            <div id="imageprojet">
                <img src="picture/Screen%20adoptundev.png" alt="image_projet">
            </div>
            <div class="contenuProjet">
                <h3>Social network</h3>
            </div>
            <button id="targetReseau">Discover</button>
        </div>
        <div id="contenuReseaux">
            <h3>Social network</h3>
            <p class="paragrapheNetwork">
                It is a personal project carried out to validate my title of web developer. <br>
            </p>
            <p class="paragrapheNetwork">
                My objective was to be able to complete as many skills as possible. So I turned to a shared content application.
                <br>
            </p>
            <p class="paragrapheNetwork">
                The languages used are HTML, CSS, PHP, SQL queries and JAVA SCRIPT. I also use the JQUERY library and AJAX methods.
                <br>
            </p>
            <p class="paragrapheNetwork" id="paragrapheSocialNetwork">
                The site continues to evolve. It took me one month to do the basics of the site. <br>
            </p>
            <a id="accesReseau" target="_blank" href="http://kevin.adoptundev.com/reseauSocial/index.php">access the site</a>
        </div>

<!--        Projet 2-->
        <div class="conteneurProjet" id="conteneurVideothèque">
            <div class="imageProjet2">
                <img src="picture/screen-video.png" alt="Videothèque">
            </div>
            <div class="titreProjet2">
                <h3>Videotheque</h3>
            </div>
            <button id="targetVideothèque">Discover</button>
        </div>
        <div id="contenuVideotheque">
            <h3>Videotheque</h3>
            <p class="paragrapheVideotheque">
                For this project, I started with an exercise during my DWWM training. <br>
            </p>
            <p class="paragrapheVideotheque">
                The goal was to work on the databases, I added content to display a film profile, its title and its synopsis.
                <br>
            </p>
            <p class="paragrapheVideotheque">
                I used HTML, CSS, PHP and SQL queries.
                <br>
            </p>
            <p class="paragrapheVideotheque" id="paragrapheVideo">
                It is an exercise that began at the beginning of the training. The estimated time is 2 days. <br>
            </p>
            <a id="accesVideotheque" target="_blank" href="http://kevin.adoptundev.com/videotheque/index.php">access the site</a>
        </div>

<!--        Projet3-->
        <div class="conteneurProjet" id="conteneurJus">
            <div class="imageProjet3">
                <img src="picture/screen-recettes.png" alt="Recettes">
            </div>
            <div class="titreProjet3">
                <h3>Juices recipes</h3>
            </div>
            <button id="targetJus">Discover</button>
        </div>
        <div id="contenuJuice">
            <h3>Juices recipes</h3>
            <p class="paragrapheJuice">
                This project starts from a theme that I have adapted to my functionalities. <br>
            </p>
            <p class="paragrapheJuice">
                I created two users with different rights (classic user (id & password: user) & administrator (id & password: admin)).
                <br>
            </p>
            <p class="paragrapheJuice">
                I used HTML, CSS, PHP and SQL queries. I am using the BOOTSTRAP theme "admin LTE" for the administrator part.
                <br>
            </p>
            <p class="paragrapheJuice" id="paragrapheJuiceRecipes">
                This exercise was done at the end of the training. I took 5 days to finish it. <br>
            </p>
            <a id="accesJuice" target="_blank" href="http://kevin.adoptundev.com/Recette-jus-de-fruits/visiteur/utilisateur.php">access the site</a>
        </div>
    </div>

</div>

<!--Footer-->
<footer>
    <h2 class="contactTitle">CONTACT</h2>
    <div id="contact">
        <p>Kevin HUERI</p>
        <p>3 rue Abel Niepce</p>
        <p>72400 La Ferté Bernard</p>
        <p>Tel.: 06 74 31 58 49</p>
        <p>Mail: kevin.hueri@gmail.com</p>
    </div>
</footer>

<script>
    // Projet 1
    $(document).ready(function(){
        $('#targetReseau').click(function(){
            $('#contenuReseaux').fadeToggle('500');
        });
    });

    // Projet 2
    $(document).ready(function(){
        $('#targetVideothèque').click(function(){
            $('#contenuVideotheque').fadeToggle('500');
        });
    });

    // Projet 3
    $(document).ready(function(){
        $('#targetJus').click(function(){
            $('#contenuJuice').fadeToggle('500');
        });
    });

    // titre
    anime.timeline({loop: true})
        .add({
            targets: '.titrePrincipal',
            scale: [0.3,1],
            opacity: [0,1],
            translateZ: 0,
            easing: "easeOutExpo",
            duration: 3000,
            delay: (el, i) => 70 * (i+1)
        }).add({
        targets: '.logo',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });

    //Sous-titre About me
    var textWrapper = document.querySelector('.aboutTitle');
    textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

    anime.timeline({loop: true})
        .add({
            targets: '.aboutTitle .letter',
            translateX: [40,0],
            translateZ: 0,
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 1200,
            delay: (el, i) => 500 + 30 * i
        }).add({
        targets: '.aboutTitle .letter',
        translateX: [0,-30],
        opacity: [1,0],
        easing: "easeInExpo",
        duration: 1100,
        delay: (el, i) => 100 + 30 * i
    });

    //Sous-titre Project
    var textWrapper2 = document.querySelector('.projectTitle');
    textWrapper2.innerHTML = textWrapper2.textContent.replace(/\S/g, "<span class='letter2'>$&</span>");

    anime.timeline({loop: true})
        .add({
            targets: '.projectTitle .letter2',
            scale: [4,1],
            opacity: [0,1],
            translateZ: 0,
            easing: "easeOutExpo",
            duration: 950,
            delay: (el, i) => 70*i
        }).add({
        targets: '.ml2',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });

    // Sous titre contact
    var textWrapper3 = document.querySelector('.contactTitle');
    textWrapper3.innerHTML = textWrapper3.textContent.replace(/\S/g, "<span class='letter3'>$&</span>");

    anime.timeline({loop: true})
        .add({
            targets: '.contactTitle .letter3',
            translateY: [-100,0],
            easing: "easeOutExpo",
            duration: 1400,
            delay: (el, i) => 30 * i
        }).add({
        targets: '.contactTitle',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });

</script>

</body>
</html>